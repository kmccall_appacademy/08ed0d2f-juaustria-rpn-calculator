class RPNCalculator
  attr_accessor :calculator

  def initialize
    @calculator = []
  end

  def push(num)
    @calculator << num
  end

  def raise_error
    raise 'calculator is empty'
  end

  def plus
    sum = @calculator.pop
    raise_error if @calculator.last.nil?
    sum += @calculator.pop
    @calculator << sum
  end

  def minus
    difference = @calculator.pop
    raise_error if @calculator.last.nil?
    difference = @calculator.pop - difference
    @calculator << difference
  end

  def times
    product = @calculator.pop
    raise_error if @calculator.last.nil?
    product *= @calculator.pop
    @calculator << product
  end

  def divide
    quotient = @calculator.pop.to_f
    raise_error if @calculator.last.nil?
    quotient = @calculator.pop.to_f / quotient.to_f
    @calculator << quotient
  end

  def value
    @calculator.last
  end

  def tokens(string)
    output = []
    string.split(' ').each do |el|
      output << if %w[+ - * /].include?(el)
                  el.to_sym
                else
                  el.to_i
                end
    end
    output
  end

  def evaluate(string)
    tokens(string).each do |el|
      if el.is_a? Integer
        push(el)
      elsif el == :+
        plus
      elsif el == :-
        minus
      elsif el == :*
        times
      elsif el == :/
        divide
      end
    end
    value
  end
end
